#!/bin/bash -eu
/usr/bin/docker stop zanata
/usr/bin/docker stop zanatadb
find /var/lib/docker/volumes/zanata-files/_data/indexes -name write.lock -delete
